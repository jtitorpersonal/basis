#Verify Mode
Add another mode where the check step is run again after the run step. 

#PEP8 Pass
Tools requires VSC, which integrates linters easily. Rename things to fit PEP8.

#Localization
All strings are assumed to be non-localized.

#Pre-specified Escalation Password
For some odd situations like brew on macOS, you might need some commands to not be sudo, which means that the implementor must be run as non-sudo but given a password when a sudo command does occur. Asking the user for a escalation password first might be useful but you need to do it in a secure way.